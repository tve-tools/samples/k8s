

## init cred registry with docker-hub login info (for private repo at docker-hub)
```bash
$ kubectl create secret generic regcred --from-file=.dockerconfigjson=/home/k8s/.docker/config.json --type=kubernetes.io/dockerconfigjson

```

`k create -f pod-grafana.yml` to create the pod...

This pod contains 2 containers: standard grafana and conf-helper which is based on [custom docker image](https://gitlab.com/tve-tools/samplesng/-/tree/master/dockerbox/grafana_provisioning).

`k logs -f grafana conf-helper`

`k create secret generic my-secret --from-literal=PASS=XXXX`


> NOT LONGER in standalone git repo - now part of the of users/k8s repo ...


